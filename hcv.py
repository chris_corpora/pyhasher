"""
Command-line program to hash, copy, and verify files in the current working directory using pyhasher.

Not tested extensively; grown out of simple program for automating the hashing, copying, and verifying files

"""
import shutil
import pyhasher
import argparse
import os
import logging
import tempfile
from datetime import datetime
import pyhasher
import hashlib
import pathlib

__license__ = """
Copyright 2017 Christopher Corpora (pyhasher@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

__version__ = "0.9.2"

__author__ = "Chris Corpora"

logger = logging.getLogger()
logfile_dir = tempfile.mkdtemp()
logfile_name = '{:%Y%m%d_%H%M%S}_HCV_Log.log'.format(datetime.now())
logfile_path = os.path.join(logfile_dir, logfile_name)

algorithms_available = {'md5', 'sha1', 'sha224','sha256','sha384','sha512'}

class ExistingDestinationError(Exception):
    pass

def hash_files(dirpath, hash_file, algorithm, ignore_system, ignore_hidden):
    """Hash files in dirpath and writes to hash_file for verification, uses pyhasher to hash files and log"""
    os.chdir(dirpath)
    args = ['-r', '-a', algorithm, '-o']
    if hash_file:
        args.append(hash_file)
    if ignore_hidden and ignore_system:
        args.append('--ignore-all')
    elif ignore_hidden:
        args.append('--ignore-hidden')
    elif ignore_system:
        args.append('--ignore-system')
    tmp = ' '.join(args)
    logging.info("Hashing files in '{src}', 'pyhasher arguments: {cargs}'".format(src=dirpath, cargs=tmp))
    cli_args = pyhasher.make_args(args)
    pyhasher.main(cli_args)

def hidden_files(path, names):
    results = []
    for fp in pathlib.Path(path).iterdir():
        if pyhasher.hidden_file(fp):
            results.append(fp)
    return results

def system_files(path, names):
    results = []
    for fp in pathlib.Path(path).iterdir():
        if pyhasher.system_file(fp):
            results.append(fp)
    return results

def hidden_and_system(path, names):
    results = []
    for fp in pathlib.Path(path).iterdir():
        if pyhasher.system_file(fp) or pyhasher.hidden_file(fp):
            results.append(fp)
    return results

def not_matching(pattern):
    def ignore_matching(path, names):
        results = []
        for fp in pathlib.Path(path).iterdir():
            if not fp.match(pattern):
                results.append(fp)
        return results

def copy_files(source_path, dest_path, ignore_system, ignore_hidden):
    """Copies all files in spath to dest_path using shutil.copytree with defaults"""
    logging.info("Copying files from '{src}' to '{dst}'".format(src=source_path, dst=dest_path))
    msg = "Set to ignore {} files and directories"
    if ignore_hidden and ignore_system:
        ig = hidden_and_system
        logging.info(msg.format("hidden and system"))
    elif ignore_hidden:
        ig = hidden_files
        logging.info(msg.format("hidden"))
    elif ignore_system:
        ig = system_files 
        logging.info(msg.format("system"))
    else:
        ig = None
    try:
        shutil.copytree(source_path, dest_path, ignore=ig)
    except shutil.Error as e:
        logging.error("Errors on Copying: {}".format(e))

def verify_files(dirpath, hash_file, md5summer=False):
    """Verifies files in dirpath using hash values in hashfile, uses pyhasher to verify"""
    os.chdir(dirpath)
    args = ['-o', '-L', '-V']
    if hash_file:
        args.append(hash_file)
        # put verification file in same directory as hash_file
        args.insert(1, os.path.dirname(hash_file))
    if md5summer:
        args.append('--md5summer')
    tmp = ' '.join(args)
    logging.info("Verifying files in '{src}', 'pyhasher arguments: {cargs}'".format(src=dirpath, cargs=tmp))
    cli_args = pyhasher.make_args(args)
    pyhasher.main(cli_args)
    
def title(text):
    top = "-"*50
    spaced = " " + text + " "
    s = [" ", top, format(spaced, "^50"), top]
    print(os.linesep.join(s))

def run(dst, source=None, hash_file=None, algorithm='md5', skip_hashing=False, md5summer=False, ignore_system=False, 
        ignore_hidden=False):
    """Runs program"""
    if source:
        source_path = os.path.abspath(source)
    else:
        source_path = os.path.abspath('.')
    dest_path = os.path.abspath(dst)
    if os.path.exists(dest_path):
        raise ExistingDestinationError
    if hash_file:
        hash_file = os.path.abspath(hash_file)
    if not skip_hashing:
        title("HCV: Hashing Files")
        hash_files(source_path, hash_file, algorithm, ignore_system, ignore_hidden)
    title("HCV: Copying Files")
    print("{} --> {}".format(source_path, dest_path))
    copy_files(source_path, dest_path, ignore_system, ignore_hidden)
    title("HCV: Verifying Files")
    verify_files(dest_path, hash_file, md5summer)
    logging.info("Completed")
    
def main(args):
    if args.ignore_all:
        args.ignore_hidden = True
        args.ignore_system = True
    run(args.destination, source=args.source,
        hash_file=args.hash_file,
        algorithm=args.algorithm,
        skip_hashing=args.skip_hashing,
        md5summer=args.md5summer,
        ignore_system=args.ignore_system,
        ignore_hidden=args.ignore_hidden)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('destination',
                        help='path to destination directory--it will be created and must not already exist')
    parser.add_argument('-s', '--source',
                        default='.',
                        help='path to source directory, default is current working directory')
    parser.add_argument('--hash-file',
                        help='path for file containing hash values, default is to create in source root directory')
    parser.add_argument('--algorithm',
                        choices=algorithms_available,
                        default='md5',
                        metavar='|'.join(algorithms_available),
                        help='algorithm to use for hashing, defaults is MD5')
    parser.add_argument('-k', '--skip-hashing', action='store_true',
                        help='Use if the files have already been hashed. Use --hash-file option if needed to specify a hash file')
    parser.add_argument('--md5summer', action='store_true',
                        help='Use if previously created hash file was produced by MD5summer program')
    parser.add_argument('--ignore-system', action='store_true',
                        help='ignore files and directories that have the system attribute')
    parser.add_argument('--ignore-hidden', action='store_true',
                        help='ignore files and directories that have the hidden attribute')
    parser.add_argument('--ignore-all', action='store_true',
                        help='ignore files and directories that have the system and hidden attribute')
    parser.add_argument('--pattern', help='pattern of files to copy and/or hash')
    args = parser.parse_args()
    LOG_FORMAT = '%(asctime)s %(message)s'
    logging.basicConfig(filename=logfile_path, filemode='a', level=logging.INFO, format=LOG_FORMAT)
    try:
        main(args)
    except ExistingDestinationError:
        logging.critical("Destination directory already exists, program did not run")
    except (OSError, PermissionError, FileNotFoundError) as e:
        logging.critical("Did not complete: {}".format(e))
    finally:
        print()
        print(60*"=")
        print()
        msg = "Path to HCV Logfile: {}".format(logfile_path)
        print("{:^60}".format(msg))
        print()
        with open(logfile_path, 'r') as fout:
            for line in fout:
                print(line)
