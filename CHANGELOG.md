## 2.7.4 ##
* Corrected header message for files that are being hashed

## 2.7.3 ##
* Bug fix for exceptions when --ignore_system and --ignore_hidden is specified on macOS

## 2.7.2 ##
* Bug fix on FileNotFoundErrors on file hashing. Seems to occur on files that exist but have long paths. Only catching the exception now.
* Added optional status display for hashing large files. Previously it always displayed the status when hashing large files

## 2.7.1 ##
* Fixed issue where contents of zip and tar files could not be hashed

## 2.7 ##
* Added --ignore-system, --ignore-hidden, and --ignore-all option to ignore system, hidden, and hidden or system files respectively.

* Changed --ignore-dir to --ignore-directory and added more tests

## 2.6 ##
* Created --ignore-dir option for ignoring a directory name for hashing; created it mostly for ignoring "System Volume Information" directory on drive root in Windows

## 2.5.2 ##

* Checked and fixed errors with Pylint for pyhasher and unit tests
* Refactored unit tests

## 2.5.1 ##

* Fixed compatibility with Python 3.6
	* pathlib.Path resolve method no longer creates a FileNotFound Error, created manually
	* Fixed issues with inferring the format from a verification file; if a separator does not contain spaces and/or asterisks, it needs to be specified
		* Inferring the format only looks for the hash algorithm, not the separator between the hash and the file path

## 2.5 ##

* Changed how output works
	* Reworked how status is displayed to make it faster, only displays the current file being hashed and the size
	* Removed option to turn off the status
	* When a log file is specified, hashes are output to the log file but still logged to standard out

* Added minimal tests for TarHasher and ZipHasher to make sure they run

## 2.4 ##

* Changed algorithms back to 'md5', 'sha1', 'sha224','sha256','sha384','sha512'

* Made it possible to turn off status display to increase speed from high bandwidth storage such as RAID Arrays

## 2.3 ##

* Removed ProgressBar

* Added status for large file reads to standard error by default, shows current file being read and percentage read as integer

## 2.2 ##

* Assigned file name patterns an option `-p` or `--pattern` instead of placing at the end optionally which causes problems when placing
after options that have optional arguments.

* Added option to only hash a single file and print to standard output

* Refactored code for parsing command line options to make it easier to test and read

## 2.0.1 ##

* Gives precedence to algorithm name in file name or inferred hash value read from file. Inferred hash algorithm from line in file will only match to guaranteed algorithms md5, sha1, sha224, sha256, and sha512 since there is overlap in lengths between algorithms.

* Fixed bug in which algorithm for verification was not being used when it was obtained from the verification filename.

## 2.0 ##

* Added all hash functions available in Python hashlib module

* Default output file extension will be .pyh instead of pymd5, pysha1, etc.

* Added --fail-fast option to stop on first hash mismatch during verification

* Removed -N/--no-dir-headings and changed default behavior for output to log the relative path of each file next to the file hash separated by two spaces

* Created option --headings that will provide previous default behavior of logging relative directory path once with filenames and hashes below.

* Changed -L/--log-output to -o/--output

* Changed option --log-all to -L/--log-all

## 1.0.2 ##
* Fixes errors in the documentation

## 1.0.1 ##
* Changes to help documentation

## 1.0 ##
* First Release